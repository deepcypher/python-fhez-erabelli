# Python-FHEz-Erabelli

Python FHEz Erabelli is a derivative of the original work of [Saroja Erabelli](https://github.com/sarojaerabelli/py-fhe)s pure python fully homomorphic encryption.

We have adapted the original work, to make it more easily distributable, to allow for expanded hosted documentation, and in general improved certain neuroses to make it all play nicely together with other backend FHEz libraries.

Please read the specific docs: https://deepcypher.gitlab.io/python-fhez-erabelli/
