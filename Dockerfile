FROM python:3-alpine

# directory containing source files with respect to dockerfile
ARG SRCDIR="."
# directory where the source will exist in the dockerfile
ARG PKGDIR="/app"

# updating and installing git for utilities like automatic versioning
RUN apk add --update --no-cache git make && \
    mkdir -p ${PKGDIR}/docs

# set out default directory (important for CMD stage in particular)
WORKDIR ${PKGDIR}

# copy in only requirements to install first
COPY ${SRCDIR}/requirements.txt ${PKGDIR}/.
COPY ${SRCDIR}/docs/requirements.txt ${PKGDIR}/docs/.
RUN pip3 install -r ${PKGDIR}/requirements.txt && \
    pip3 install -r ${PKGDIR}/docs/requirements.txt

# copy in everything else after requirements to split up and reduce load on
# host servers as this will inevitably be changed far more than requirements
COPY ${SRCDIR} ${PKGDIR}
RUN python3 ${PKGDIR}/setup.py install

# # default command to run when using docker run
# CMD python3 ./examples/ckks_mult_example.py
