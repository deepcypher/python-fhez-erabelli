import re
import subprocess
from setuptools import setup, find_packages, find_namespace_packages


def get_git_version():
    """Get the version from git describe in archlinux format."""
    try:
        # getting version from git as this is vcs
        # below equivelant or achlinux versioning scheme:
        # git describe --long | sed 's/\([^-]*-\)g/r\1/;s/-/./g
        git_describe = subprocess.Popen(
            ["git", "describe", "--long"],
            stdout=subprocess.PIPE)
        version_num = subprocess.check_output(
            ["sed", r"s/\([^-]*-\)g/r\1/;s/-/./g"],
            stdin=git_describe.stdout)
        git_describe.wait()
        version_git = version_num.decode("ascii").strip()

    except subprocess.CalledProcessError:
        # for those who do not have git or sed availiable (probably non-linux)
        # this is tricky to handle, lots of suggestions exist but none that
        # neither require additional library or subprocessess
        version_git = "0.0.0"  # for now we will provide a number for you
    return version_git


def get_requirements(path=None):
    """get a list of requirements and any dependency links associated.

    This function fascilitates git urls being in requirements.txt
    and installing them as normall just like pip install -r requirements.txt
    would but setup does not by default.

    :e.g requirements.txt\::

        ConfigArgParse
        git+https://github.com/DreamingRaven/python-ezdb.git#branch=master

    should result in

    :requirements\::

        [
            ConfigArgParse
            python-ezdb
        ]

    : dependenc links\::

        ["git+https://github.com/DreamingRaven/python-ezdb.git#egg=python-ezdb"]

    version can also be appended but that may break compatibility with
    pip install -r requirements.txt so we will not attempt that here but would
    look something like this:

    "git+https://github.com/DreamingRaven/python-ezdb.git#egg=python-ezdb-0.0.1"
    """
    dependency_links = []

    #  read in the requirements file
    path = path if path is not None else "./requirements.txt"
    with open(path, "r") as f:
        requirements = f.read().splitlines()

    # apply regex to find all desired groups like package name and url
    re_git_url = r"^\bgit.+/(.+)\.git"
    re_groups = list(map(lambda x: re.search(re_git_url, x), requirements))

    # iterate over regex and select package name group to insert over url
    for i, content in enumerate(re_groups):
        # re.search can return None so only if it returned something
        if content:
            print(i, content, requirements[i])
            requirements[i] = content.group(1)
            dependency_links.append("{}#egg={}".format(content.group(0),
                                                       content.group(1)))

    return requirements, dependency_links


with open("README.md", "r") as fh:
    readme = fh.read()

dependencies, links = get_requirements()
version = get_git_version()
packages = find_namespace_packages(
    exclude=("docs", "docs.*", "examples", "examples.*",  # "tests", "tests.*",
             "build", "build.*", "dist", "dist.*", "venv", "venv.*"))

print("version:", version)
print("requirements:", dependencies)
print("requirement_links:", links)
print("namespace packages:", packages)

setup(
    name='python-fhez-erabelli',
    version=version,
    description='Python implementations of CKKS and BFV cryptosystems',
    long_description=readme,
    long_description_content_type="text/markdown",
    author=['Saroja Erabelli', "George Onoufriou"],
    url="https://gitlab.com/deepcypher/python-fhez-erabelli",
    license='MIT',
    install_requires=dependencies,  # gets requirements from requirements.txt
    dependency_links=links,  # allows full requirements.txt functionality
    packages=packages
)
