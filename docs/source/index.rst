.. pyrtd documentation master file, created by
   sphinx-quickstart on Mon Aug 26 13:30:29 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. include:: substitutions

python-fhez-erabelli
====================


.. toctree::
  :maxdepth: 1
  :caption: Table of Contents
  :numbered:

  license
  install
  examples
  ckks
  bfv
  util
