Usage Examples
==============

CKKS
----

.. literalinclude:: ../../examples/ckks_mult_example.py
  :language: python

BFV
---

.. literalinclude:: ../../examples/bfv_mult_example.py
  :language: python
