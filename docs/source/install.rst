.. include:: substitutions

Install
=======

There are a few different methods of installation possible for this code depending on your needs.

Docker
------

You can run one of our automatically-built docker containers interactively, directly from our |docker registry|_:

.. code-block:: bash

  docker run -it registry.gitlab.com/deepcypher/python-fhez-erabelli:master

Alternatively you can git clone the repository, and build the docker container locally:

.. code-block:: bash

  git clone https://gitlab.com/deepcypher/python-fhez-erabelli erabelli
  cd erabelli
  docker build -t fhez/erabelli -f Dockerfile .

Then you can run the container using the same name we tagged (-t) the image with:

.. code-block:: bash

  docker run -it fhez/erabelli

Pip
---

To install this package to the local system simply:

.. code-block:: bash

  pip install git+https://gitlab.com/deepcypher/python-fhez-erabelli.git#branch=master
